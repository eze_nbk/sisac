package com.escobar.sisac.repository

import co.newbrick.usoundkitpro.nsplib.NspLib
import dagger.Module
import dagger.Provides

@Module
class Repository : NspLib.Repository{

    @Provides
    fun getInstance() = this
}