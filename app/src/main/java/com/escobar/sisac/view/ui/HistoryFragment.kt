package com.escobar.sisac.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.escobar.sisac.R
import com.escobar.sisac.databinding.FragmentHistoryBinding
import com.escobar.sisac.viewModel.SisacViewModel

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class HistoryFragment : Fragment() {

    private var _binding: FragmentHistoryBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    private val sisacViewModel: SisacViewModel by lazy {
        ViewModelProvider(requireActivity())[SisacViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, OnBackHandler {
            findNavController().popBackStack(R.id.MainFragment, false)
        })

        with(sisacViewModel.getHistory()) {
            if (this.isNotEmpty()) {
                binding.tvHistory.visibility = VISIBLE
                binding.tvHistory.text = "- ${this.joinToString("\n- ")}"
                binding.fhyClEmptyList.visibility = INVISIBLE
            } else {
                binding.tvHistory.visibility = INVISIBLE
                binding.fhyClEmptyList.visibility = VISIBLE
            }
        }

        binding.fhyBBack.setOnClickListener {
            findNavController().popBackStack(R.id.MainFragment, false)
        }
    }

    inner class OnBackHandler(val callback: () -> Unit) : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() = callback.invoke()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}