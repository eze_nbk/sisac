package com.escobar.sisac.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import com.escobar.sisac.R
import com.escobar.sisac.databinding.FragmentMainBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.mainBGestures.setOnClickListener {
            findNavController(it).navigate(R.id.action_MainFragment_to_scanFragment)
        }

        binding.mainBObjects.setOnClickListener {
            findNavController(it).navigate(R.id.action_MainFragment_to_takePhotoFragment)
        }

        binding.mainBShowHistory.setOnClickListener {
            findNavController(it).navigate(R.id.action_MainFragment_to_historyFragment)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}