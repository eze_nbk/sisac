package com.escobar.sisac.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.escobar.sisac.R
import com.escobar.sisac.databinding.FragmentThreatBinding
import com.escobar.sisac.utility.RedAlert
import com.escobar.sisac.viewModel.SisacViewModel
import com.google.gson.annotations.SerializedName
import java.time.LocalDate
import java.time.LocalTime
import java.util.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ThreatFragment : Fragment() {

    private var _binding: FragmentThreatBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val sisacViewModel: SisacViewModel by lazy {
        ViewModelProvider(requireActivity())[SisacViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentThreatBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner, OnBackHandler {
            findNavController().popBackStack(R.id.MainFragment, false)
        })

        sisacViewModel.resetGesture()

        val year = LocalDate.now().year
        val month = LocalDate.now().monthValue
        val day = LocalDate.now().dayOfMonth

        val hour = LocalTime.now().hour
        val minute = LocalTime.now().minute

        binding.fthTvDate.text = String.format("Fecha: %s/%s/%s", day, month, year)
        binding.fthTvTime.text = String.format("Hora: %s:%s", hour, minute)

        sisacViewModel.currentRiskLD.observe(viewLifecycleOwner) {
            binding.fthTvRiskLevel.text = "RIESGO $it"
            val threatList = sisacViewModel.getHistory()
            threatList.add("Detección = Tipo:${sisacViewModel.eventType}, Riesgo: $it, ${binding.fthTvDate.text}, ${binding.fthTvTime.text}")
            sisacViewModel.setHistory(threatList)
            sisacViewModel.sendToServer(RedAlert(
                type = "RIESGO $it",
                location = "Namaste Store, Undiano 886, S. S. de Jujuy",
                sender = "Mayra Izzeta",
                dni = "32339337"
            ))
        }

        sisacViewModel.currentBitmapLD.observe(viewLifecycleOwner) {
            binding.fthPreview.setImageBitmap(it)
        }

        sisacViewModel.sendSuccessLD.observe(viewLifecycleOwner) {
            if(it.isNotEmpty()) {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                sisacViewModel.resetSendStatus()
            }
        }

        binding.fthBBack.setOnClickListener {
            findNavController().popBackStack(R.id.MainFragment, false)
        }

    }

    inner class OnBackHandler(val callback: () -> Unit) : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() = callback.invoke()
    }

}
