package com.escobar.sisac.view.ui

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.escobar.sisac.R
import com.escobar.sisac.databinding.FragmentTakePhotoBinding
import com.escobar.sisac.viewModel.SisacViewModel
import com.otaliastudios.cameraview.CameraListener
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.task.vision.detector.ObjectDetector
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class TakePhotoFragment : Fragment() {

    private lateinit var currentPhotoPath: String
    private lateinit var inputImageView: ImageView

    private var _binding: FragmentTakePhotoBinding? = null

    private val sisacViewModel: SisacViewModel by lazy {
        ViewModelProvider(requireActivity())[SisacViewModel::class.java]
    }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentTakePhotoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ftpIvIcon.setOnClickListener {
            binding.cvCamera.playSounds = false
            binding.cvCamera.capturePicture()
        }

        binding.ftpBTurnOff.setOnClickListener {
            findNavController().popBackStack(R.id.MainFragment, false)
        }

        binding.cvCamera.apply {
            setLifecycleOwner(viewLifecycleOwner)
            addCameraListener(object : CameraListener() {
                override fun onPictureTaken(jpeg: ByteArray?) {
                    jpeg?.let {
                        getCapturedImage(jpeg).let { bitmap ->
                            binding.imageView.setImageBitmap(bitmap)
                            setViewAndDetect(bitmap)
                        }
                    }
                }
            })
        }

        inputImageView = binding.imageView

    }

    /**
     * getCapturedImage():
     *     Decodes and crops the captured image from camera.
     */
    private fun getCapturedImage(byteArray: ByteArray): Bitmap {
        // Get the dimensions of the View
        val targetW: Int = inputImageView.width
        val targetH: Int = inputImageView.height

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true

            BitmapFactory.decodeByteArray(byteArray,0 , byteArray.size, this)

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            // Determine how much to scale down the image
            val scaleFactor: Int = max(1, min(photoW / targetW, photoH / targetH))

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
            inMutable = true
        }
//        val exifInterface = ExifInterface(currentPhotoPath)
//        val orientation = exifInterface.getAttributeInt(
//            ExifInterface.TAG_ORIENTATION,
//            ExifInterface.ORIENTATION_UNDEFINED
//        )

        val bitmap = BitmapFactory.decodeByteArray(byteArray,0 , byteArray.size, bmOptions)
//        return when (orientation) {
//            ExifInterface.ORIENTATION_ROTATE_90 -> {
                return rotateImage(bitmap, 90f)
//            }
//            ExifInterface.ORIENTATION_ROTATE_180 -> {
//                rotateImage(bitmap, 180f)
//            }
//            ExifInterface.ORIENTATION_ROTATE_270 -> {
//                rotateImage(bitmap, 270f)
//            }
//            else -> {
//                bitmap
//            }
//        }
    }

    /**
     * Set image to view and call object detection
     */
    private fun setViewAndDetect(bitmap: Bitmap) {
        // Display the captured image
        inputImageView.setImageBitmap(bitmap)

        // Run object detection and display the result
        runObjectDetection(bitmap)
    }

    private fun runObjectDetection(bitmap: Bitmap) {
        val image = TensorImage.fromBitmap(bitmap)
        val options = ObjectDetector.ObjectDetectorOptions.builder()
            .setMaxResults(1)
            .setScoreThreshold(0.5f)
            .build()
        val detector = ObjectDetector.createFromFileAndOptions(
            requireActivity(),
            "custom_models/guns.tflite",
            options
        )
        val results = detector.detect(image)
        var hasPistols : Boolean = false
        val resultToDisplay = results.map {
            val category = it.categories.first()
            hasPistols = category.label == "pistol"
            val text = "${category.label}, ${category.score.times(100)}"
            BoxWithText(it.boundingBox, text)
        }
        val imgWithResult = drawDetectionResult(bitmap, resultToDisplay)
        requireActivity().runOnUiThread {
            inputImageView.setImageBitmap(imgWithResult)
                if(hasPistols) {
                    sisacViewModel.gunDetected()
                    sisacViewModel.setCurrentBitmap(imgWithResult)
                    Handler(Looper.getMainLooper()).postDelayed({
                        findNavController().navigate(R.id.action_takePhotoFragment_to_threatFragment)
                    }, 3000)
                }
            }

    }

    /**
     * Draw bounding boxes around objects together with the object's name.
     */
    private fun drawDetectionResult(
        bitmap: Bitmap,
        detectionResults: List<BoxWithText>
    ): Bitmap {
        val outputBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(outputBitmap)
        val pen = Paint()
        pen.textAlign = Paint.Align.LEFT

        detectionResults.forEach {
            // draw bounding box
            pen.color = Color.RED
            pen.strokeWidth = 8F
            pen.style = Paint.Style.STROKE
            val box = it.box
            canvas.drawRect(box, pen)

            val tagSize = Rect(0, 0, 0, 0)

            // calculate the right font size
            pen.style = Paint.Style.FILL_AND_STROKE
            pen.color = Color.YELLOW
            pen.strokeWidth = 2F

            pen.textSize = MAX_FONT_SIZE
            pen.getTextBounds(it.text, 0, it.text.length, tagSize)
            val fontSize: Float = pen.textSize * box.width() / tagSize.width()

            // adjust the font size so texts are inside the bounding box
            if (fontSize < pen.textSize) pen.textSize = fontSize

            var margin = (box.width() - tagSize.width()) / 2.0F
            if (margin < 0F) margin = 0F
            canvas.drawText(
                it.text, box.left + margin,
                box.top + tagSize.height().times(1F), pen
            )
        }
        return outputBitmap
    }

    /**
     * Rotate the given bitmap.
     */
    private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    /**
     * A general-purpose data class to store detection result for visualization
     */
    data class BoxWithText(val box: RectF, val text: String)


    private fun showPreview() {
        binding.imageView.visibility = VISIBLE
        binding.cvCamera.visibility = GONE
    }

    private fun hidePreview() {
        binding.imageView.visibility = GONE
        binding.cvCamera.visibility = VISIBLE
    }

    companion object {
        private const val OBJECT_DETECTION = "Object Detection"
        private const val OBJECT_DETECTION_CUSTOM = "Custom Object Detection"
        private const val POSE_DETECTION = "Pose Detection"

        private const val TAG = "LivePreviewActivity"
        private const val PERMISSION_REQUESTS = 1

        //val TAG = "MLKit-ODT"
        val REQUEST_IMAGE_CAPTURE: Int = 1
        private val MAX_FONT_SIZE = 96F

        private fun isPermissionGranted(context: Context, permission: String?): Boolean {
            if (ContextCompat.checkSelfPermission(context, permission!!) ==
                PackageManager.PERMISSION_GRANTED
            ) {
                Log.i(TAG, "Permission granted: $permission")
                return true
            }
            Log.i(TAG, "Permission NOT granted: $permission")
            return false
        }
    }
}