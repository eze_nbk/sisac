/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.mlkit.vision.demo.kotlin.posedetector

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.escobar.sisac.activity.SisacActivity
import com.escobar.sisac.view.ui.custom.GraphicOverlay
import com.escobar.sisac.view.ui.custom.PoseClassifierProcessor
import com.escobar.sisac.view.ui.custom.VisionProcessorBase
import com.escobar.sisac.viewModel.SisacViewModel
import com.google.android.gms.tasks.Task
import com.google.android.odml.image.MlImage
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.pose.*
import java.util.ArrayList
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import kotlin.math.abs
import kotlin.math.atan2

/** A processor to run pose detector. */
class PoseDetectorProcessor(
  private val context: Context,
  options: PoseDetectorOptionsBase,
  private val showInFrameLikelihood: Boolean,
  private val visualizeZ: Boolean,
  private val rescaleZForVisualization: Boolean,
  private val runClassification: Boolean,
  private val isStreamMode: Boolean
) : VisionProcessorBase<PoseDetectorProcessor.PoseWithClassification>(context) {

  private val detector: PoseDetector
  private val classificationExecutor: Executor

  private var poseClassifierProcessor: PoseClassifierProcessor? = null

  /** Internal class to hold Pose and classification results. */
  class PoseWithClassification(val pose: Pose, val classificationResult: List<String>)

  init {
    detector = PoseDetection.getClient(options)
    classificationExecutor = Executors.newSingleThreadExecutor()
  }

  override fun stop() {
    super.stop()
    detector.close()
  }

  override fun detectInImage(image: InputImage): Task<PoseWithClassification> {
    return detector
      .process(image)
      .continueWith(
        classificationExecutor,
        { task ->
          val pose = task.getResult()
          var classificationResult: List<String> = ArrayList()
          if (runClassification) {
            if (poseClassifierProcessor == null) {
              poseClassifierProcessor = PoseClassifierProcessor(context, isStreamMode)
            }
            classificationResult = poseClassifierProcessor!!.getPoseResult(pose)
          }
          PoseWithClassification(pose, classificationResult)
        }
      )
  }


  override fun detectInImage(image: MlImage): Task<PoseWithClassification> {
    return detector
      .process(image)
      .continueWith(
        classificationExecutor,
        { task ->
          val pose = task.getResult()
          var classificationResult: List<String> = ArrayList()
          if (runClassification) {
            if (poseClassifierProcessor == null) {
              poseClassifierProcessor = PoseClassifierProcessor(context, isStreamMode)
            }
            classificationResult = poseClassifierProcessor!!.getPoseResult(pose)
          }
          detectRiskGesture(context, pose)
          PoseWithClassification(pose, classificationResult)
        }
      )
  }

  var midRiskCount = 0
  var highRiskCount = 0

  private fun detectRiskGesture(context: Context, pose: Pose) {

    val rightHandAngle : Double? = findRightHand(pose)?.let { rightHand ->
      pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER)?.let { rightShoulder ->
        pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER)?.let { leftShoulder ->
          getAngle(rightHand, rightShoulder, leftShoulder)
        }
      }
    }

    val leftHandAngle : Double? = findLeftHand(pose)?.let { leftHand ->
      pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER)?.let { rightShoulder ->
        pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER)?.let { leftShoulder ->
          getAngle(rightShoulder, leftShoulder, leftHand)
        }
      }
    }

    val rightArmpitAngle : Double? = pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER)?.let { rightShoulder ->
      pose.getPoseLandmark(PoseLandmark.RIGHT_ELBOW)?.let { rightElbow ->
        pose.getPoseLandmark(PoseLandmark.RIGHT_HIP)?.let { rightHip ->
          getAngle(rightElbow, rightShoulder, rightHip)
        }
      }
    }

    val leftArmpitAngle : Double? = pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER)?.let { leftShoulder ->
      pose.getPoseLandmark(PoseLandmark.LEFT_ELBOW)?.let { leftElbow ->
        pose.getPoseLandmark(PoseLandmark.LEFT_HIP)?.let { leftHip ->
          getAngle(leftElbow, leftShoulder, leftHip)
        }
      }
    }

    when {
      leftHandAngle != null
              && leftHandAngle < 180.0
              && leftArmpitAngle != null
              && leftArmpitAngle > 45.0
              && rightHandAngle != null
              && rightHandAngle < 180.0
              && rightArmpitAngle != null
              && rightArmpitAngle > 45.0 -> {
        Log.e("POSE_DETECTION", "BRAZOS ARRIBA")
        if(midRiskCount > 10) {
          midRiskCount = 0
          getSisacViewModel(context).gestureDetected("BRAZOS ARRIBA", "ALTO")
        } else midRiskCount++
        highRiskCount = 0
      }
      rightHandAngle != null
              && rightHandAngle < 180.0
              && rightArmpitAngle != null
              && rightArmpitAngle > 45.0 -> {
        Log.e("POSE_DETECTION", "BRAZO DERECHO ARRIBA")
        if(highRiskCount > 10) {
          highRiskCount = 0
          getSisacViewModel(context).gestureDetected("BRAZO DERECHO ARRIBA", "MEDIO")
        } else highRiskCount++
        midRiskCount = 0
      }

    }
  }

  var sisacViewModel : SisacViewModel? = null

  private fun getSisacViewModel(context: Context) : SisacViewModel {
    return sisacViewModel ?: let {
      sisacViewModel = ViewModelProvider(context as SisacActivity).get(SisacViewModel::class.java)
      sisacViewModel!!
    }
  }

  private fun findRightHand(pose: Pose) : PoseLandmark? =
    pose.getPoseLandmark(PoseLandmark.RIGHT_INDEX)
      ?: pose.getPoseLandmark(PoseLandmark.RIGHT_THUMB)
      ?: pose.getPoseLandmark(PoseLandmark.RIGHT_WRIST)
      ?: pose.getPoseLandmark(PoseLandmark.RIGHT_PINKY)

  private fun findLeftHand(pose: Pose) : PoseLandmark? =
    pose.getPoseLandmark(PoseLandmark.LEFT_INDEX)
      ?: pose.getPoseLandmark(PoseLandmark.LEFT_THUMB)
      ?: pose.getPoseLandmark(PoseLandmark.LEFT_WRIST)
      ?: pose.getPoseLandmark(PoseLandmark.LEFT_PINKY)


  fun getAngle(firstPoint: PoseLandmark, midPoint: PoseLandmark, lastPoint: PoseLandmark): Double {
    var result = Math.toDegrees(atan2((lastPoint.position.y - midPoint.position.y).toDouble(),
      (lastPoint.position.x - midPoint.position.x).toDouble())
            - atan2(firstPoint.position.y - midPoint.position.y,
      firstPoint.position.x - midPoint.position.x))
    result = abs(result) // Angle should never be negative
    if (result > 180) {
      result = 360.0 - result // Always get the acute representation of the angle
    }
    return result
  }



  override fun onSuccess(
    poseWithClassification: PoseWithClassification,
    graphicOverlay: GraphicOverlay
  ) {
    graphicOverlay.add(
      PoseGraphic(
        graphicOverlay,
        poseWithClassification.pose,
        showInFrameLikelihood,
        visualizeZ,
        rescaleZForVisualization,
        poseWithClassification.classificationResult
      )
    )
  }

  override fun onFailure(e: Exception) {
    Log.e(TAG, "Pose detection failed!", e)
  }

  override fun isMlImageEnabled(context: Context?): Boolean {
    // Use MlImage in Pose Detection by default, change it to OFF to switch to InputImage.
    return true
  }

  companion object {
    private val TAG = "PoseDetectorProcessor"
  }
}
