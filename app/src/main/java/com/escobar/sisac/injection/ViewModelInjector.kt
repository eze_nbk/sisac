package com.escobar.sisac.injection

import com.escobar.sisac.repository.Repository
import com.escobar.sisac.viewModel.SisacViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [Repository::class])
interface ViewModelInjector {
    fun inject(sisacViewModel: SisacViewModel)
}