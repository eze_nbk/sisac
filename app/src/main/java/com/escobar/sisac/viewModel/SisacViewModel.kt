package com.escobar.sisac.viewModel

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.newbrick.usoundkitpro.nsplib.nsp.ResponseStatus
import co.newbrick.usoundkitpro.nsplib.post.StandardSuccessResponse
import co.newbrick.usoundkitpro.nsplib.utils.GsonParser
import com.escobar.sisac.injection.DaggerViewModelInjector
import com.escobar.sisac.repository.Repository
import com.escobar.sisac.utility.RedAlert
import com.escobar.sisac.utility.Resources
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class SisacViewModel : ViewModel() {

    @Inject
    lateinit var repo: Repository

    private val serverPoster by lazy { repo.getNspLib().getServerPoster() }

    private val gestureDetectedMLD = MutableLiveData<String>()
    val gestureDetectedLD : LiveData<String> get() = gestureDetectedMLD

    private val currentRiskMLD = MutableLiveData<String>()
    val currentRiskLD : LiveData<String> get() = currentRiskMLD

    private val currentBitmapMLD = MutableLiveData<Bitmap>()
    val currentBitmapLD : LiveData<Bitmap> get() = currentBitmapMLD

    private var history = mutableListOf<String>()
    var eventType = ""

    private val sendSuccessMLD = MutableLiveData<String>()
    val sendSuccessLD : LiveData<String> = sendSuccessMLD

    init {
        DaggerViewModelInjector.create().inject(this)
    }

    fun gestureDetected(gesture: String, risk: String) {
        Log.d("POSE_DETECTION", "$gesture -> $risk")
        val str = "$gesture -> $risk"
        eventType = "Gesto"
        gestureDetectedMLD.postValue(str)
        currentRiskMLD.postValue(risk)
    }

    fun gunDetected() {
        currentRiskMLD.postValue("ALTO")
        eventType = "Pistola"
    }

    fun setCurrentBitmap(bitmap: Bitmap) {
        currentBitmapMLD.postValue(bitmap)
    }

    fun setHistory(hist: MutableList<String>) {
        history = hist
        Resources.sharedPrefs.putString("threatsFound", GsonParser.toJson(hist))
    }

    fun getHistory() : MutableList<String> = if(history.isEmpty()) GsonParser.fromJson(
        Resources.sharedPrefs.getString(
            "threatsFound",
            GsonParser.toJson(mutableListOf<String>())
        )!!
    ) else history

    fun resetGesture() {
        gestureDetectedMLD.postValue("")
    }

    fun resetSendStatus() {
        sendSuccessMLD.postValue("")
    }

    fun sendToServer(redAlert: RedAlert) = CoroutineScope(Dispatchers.IO).launch {
        postAlertToServerServer(redAlert) {
            sendSuccessMLD.postValue(
                if(it.isSuccessful()) "alerta enviada con éxito"
                else "error de conexión, no se ha podido enviar la alerta"
            )
        }
    }

    private fun postAlertToServerServer(
        redAlert: RedAlert,
        callback: (ResponseStatus<StandardSuccessResponse>) -> Unit
    ) = serverPoster.post(
        serverUrl = "https://testing-stock-service.usound.co/inventory/api/alert/",
        obj = redAlert,
        callback = callback
    )
}