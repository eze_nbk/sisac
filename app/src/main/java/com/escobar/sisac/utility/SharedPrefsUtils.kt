package com.escobar.sisac.utility

import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import java.lang.Double.*

@Module
class SharedPrefsUtils(private val sharedPrefs: SharedPreferences) {

    /**
     * @DI @Provides injector with its instance for external classes
     */
    @Provides
    fun getPrefs() = this


    /**
     * Saves a boolean into SharedPreferences
     *
     * @param key tag wordGroupName
     * @param value to be stored
     * @return Returns a reference to the same Editor object, so you can chain put calls together.
     */
    fun putBoolean(key: String, value: Boolean) = sharedPrefs.edit().putBoolean(key, value).apply()

    /**
     * Gets a boolean from SharedPreferences
     *
     * @param key tag wordGroupName
     * @return stored value or false if not found
     */
    fun getBoolean(key: String) = sharedPrefs.getBoolean(key, false)


    /**
     * Gets a boolean from SharedPreferences
     *
     * @param key tag wordGroupName
     * * @param def default boolean
     * @return stored value or a defaultValue if not found
     */
    fun getBoolean(key: String, def: Boolean) = sharedPrefs.getBoolean(key, def)

    /**
     * Saves a String into SharedPreferences
     *
     * @param key tag wordGroupName
     * @param value to be stored
     * @return Returns a reference to the same Editor object, so you can chain put calls together.
     */
    fun putString(key: String, value: String) = sharedPrefs.edit().putString(key, value).apply()

    /**
     * Gets a String from SharedPreferences
     *
     * @param key tag wordGroupName
     * @return stored value or empty String if not found
     */
    fun getString(key: String) : String? = sharedPrefs.getString(key, "")


    /**
     * Gets a String from SharedPreferences
     *
     * @param key tag wordGroupName
     * @param def default string
     * @return stored value or a default value if not found
     */
    fun getString(key: String, def: String?): String? = sharedPrefs.getString(key, def)

    /**
     * Saves an integer into SharedPreferences
     *
     * @param key tag wordGroupName
     * @param value to be stored
     * @return Returns a reference to the same Editor object, so you can chain put calls together.
     */
    fun putInt(key: String, value: Int) = sharedPrefs.edit().putInt(key, value).apply()

    /**
     * Gets an integer from S
     * haredPreferences
     *
     * @param key tag wordGroupName
     * @return stored value or 0 if not found
     */
    fun getInt(key: String) = sharedPrefs.getInt(key, 0)

    /**
     * Gets an integer from SharedPreferences
     *
     * @param key tag wordGroupName
     * @param def default Int
     * @return stored value or a default value if not found
     */
    fun getInt(key: String, def: Int) = sharedPrefs.getInt(key, def)

    /**
     * Saves a long into SharedPreferences
     *
     * @param key tag wordGroupName
     * @param value to be stored
     * @return Returns a reference to the same Editor object, so you can chain put calls together.
     */
    fun putLong(key: String, value: Long) = sharedPrefs.edit().putLong(key, value).apply()

    /**
     * Gets a long from SharedPreferences
     *
     * @param key tag wordGroupName
     * @return stored value or 0 if not found
     */
    fun getLong(key: String) = sharedPrefs.getLong(key, 0)

    /**
     * Gets a long from SharedPreferences
     *
     * @param key tag wordGroupName
     * @param def default string
     * @return stored value or a default value if not found
     */
    fun getLong(key: String, def: Long) = sharedPrefs.getLong(key, def)

    /**
     * Gets a double from SharedPreferences
     *
     * @param key tag wordGroupName
     * @return Returns a reference to the same Editor object, so you can chain put calls together.
     */
    fun putDouble(key: String, value: Double) = sharedPrefs.edit()
            .putLong(key, doubleToRawLongBits(value))


    /**
     * Gets a double from SharedPreferences
     *
     * @param key tag wordGroupName
     * @param def default string
     * @return stored value or a default value if not found
     */
    fun getDouble(key: String, def: Double) =
            longBitsToDouble(sharedPrefs.getLong(key, doubleToLongBits(def)))

    /**
     * Gets a float from SharedPreferences
     *
     * @param key tag wordGroupName
     * @param def default float
     * @return stored value or a default value if not found
     */
    fun getFloat(key: String, def: Float) = sharedPrefs.getFloat(key, def)

    /**
     * Saves a float into SharedPreferences
     *
     * @param key tag wordGroupName
     * @param value to be stored
     * @return Returns a reference to the same Editor object, so you can chain put calls together.
     */
    fun putFloat(key: String, value: Float) = sharedPrefs.edit().putFloat(key, value).apply()

    fun removePreference(key: String) = sharedPrefs.edit().remove(key)

}