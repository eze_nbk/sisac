package com.escobar.sisac.utility

import com.google.gson.annotations.SerializedName

class RedAlert(
    @SerializedName("alert_type")
    val type : String = "",
    @SerializedName("alert_location")
    val location : String = "",
    @SerializedName("alert_sender")
    val sender : String = "",
    @SerializedName("alert_sender_dni")
    val dni : String = ""
)