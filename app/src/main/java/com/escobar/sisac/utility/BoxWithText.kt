package com.escobar.sisac.utility

import android.graphics.Rect

data class BoxWithText(val box: Rect, val text: String)