package co.newbrick.usoundkitpro.nsplib.utils

import com.google.gson.*
import com.google.gson.reflect.TypeToken

object GsonParser {
    var gson: Gson = GsonBuilder().create()

    //    inline fun <reified T> toJson(src: T): String = Gson().toJson(src)
    fun <T> toJson(src: T) : String = src?.let {
        gson.toJson(src, object : TypeToken<T>() {}.type)
    } ?: gson.toJson(JsonNull.INSTANCE)

    inline fun <reified T> fromJson(src: String): T =
        gson.fromJson(src, object : TypeToken<T>() {}.type)


}
