package co.newbrick.usoundkitpro.nsplib.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.telephony.TelephonyManager
import android.util.DisplayMetrics
import java.io.File
import java.util.*

object ResourceProvider {

    val instance = this

    //DEBUG
    const val LICENSE_SERVER_DEBUG_URL = "https://testing-pay-license.usound.co/license/check-json"
    const val APPSTORE_SERVER_DEBUG_URL = "https://appstore.usound.co/check-json"
    const val TRACKING_SERVER_DEBUG_URL = "https://testing-kit-pro-backend-internal.usound.co/checkjson-v2"
    //RELEASE
    const val LICENSE_SERVER_RELEASE_URL = "https://usound-pay-license.usound.co/license/check-json"
    const val APPSTORE_SERVER_RELEASE_URL = "https://appstore.usound.co/check-json"
    const val TRACKING_SERVER_RELEASE_URL = "https://kit-pro-backend.usound.co/checkjson-v2"

    lateinit var uniqueUUID: String
        private set
    lateinit var packageName: String
        private set
    lateinit var versionName: String
        private set
    var versionCode = 0
        private set
    lateinit var displayMetrics: DisplayMetrics
        private set
    lateinit var configuration: Configuration
        private set
    lateinit var networkCountryIso: String
        private set
    lateinit var networkOperatorName: String
        private set
    lateinit var simCountryIso: String
        private set
    lateinit var simOperatorName: String
        private set
    var simState = -1
    const val deviceId = "empty"
    const val serialNumber = "empty"
    const val simSubscrib = "empty"



    lateinit var externalFilesDir : File

    fun prepareResources(context: Context) {
        uniqueUUID = DeviceUuidFactory.calcUniqueIdHandle(context)
        packageName = context.packageName
        val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        @Suppress("DEPRECATION")
        versionCode = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            packageInfo.longVersionCode.toInt()
        } else packageInfo.versionCode
        versionName = packageInfo.versionName
        displayMetrics = context.resources.displayMetrics
        configuration = context.resources.configuration
        (context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).let {
            try {
                networkCountryIso = it.networkCountryIso
                networkOperatorName = it.networkOperatorName
                simCountryIso = it.simCountryIso
                simOperatorName = it.simOperatorName
                simState = it.simState
            } catch (e: SecurityException) {
                networkCountryIso = "xx"
                networkOperatorName = "empty"
                simCountryIso = "xx"
                simOperatorName = "empty"
                simState = 1
            }
        }
    }

    fun getLanguage(): String = try {
        configuration.locales.get(0).isO3Language
    } catch (e: MissingResourceException) {
        e.printStackTrace()
        "empty"
    }

    fun getCountry(): String = try {
        configuration.locales.get(0).isO3Country
    } catch (e: MissingResourceException) {
        e.printStackTrace()
        "empty"
    }
}