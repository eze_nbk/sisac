package co.newbrick.usoundkitpro.nsplib

import co.newbrick.usoundkitpro.nsplib.post.ServerPoster

class NspLib: ServerPoster.Repository {

    interface Repository {
        fun getNspLib() = NspLib()
    }

}
