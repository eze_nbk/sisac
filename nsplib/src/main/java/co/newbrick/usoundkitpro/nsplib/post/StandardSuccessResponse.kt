package co.newbrick.usoundkitpro.nsplib.post

import com.google.gson.annotations.SerializedName

data class StandardSuccessResponse(
    @SerializedName("success") val success: Boolean = false
)
