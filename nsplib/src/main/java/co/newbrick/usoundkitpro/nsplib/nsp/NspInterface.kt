package co.newbrick.usoundkitpro.nsplib.nsp

import co.newbrick.usoundkitpro.nsplib.nsp.client.ClientBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Callback

interface NspInterface {

    fun post(
        serverUrl: String,
        json: String,
        callback: Callback<ResponseBody>
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val fileUrl = serverUrl.split("/").let { it[it.lastIndex] }
            val baseUrl = serverUrl.replace(fileUrl, "")
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            NspApi
                .create(
                    baseUrl = baseUrl,
                    client = ClientBuilder().build( interceptor = logging, isDownload = false)
                )
                .post(fileUrl, body = json.toRequestBody())
                .enqueue(callback)
        }
    }

}