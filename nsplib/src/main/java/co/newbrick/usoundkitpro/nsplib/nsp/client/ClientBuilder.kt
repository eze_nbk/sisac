package co.newbrick.usoundkitpro.nsplib.nsp.client

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

internal class ClientBuilder {
    fun build(interceptor: okhttp3.Interceptor?, isDownload: Boolean) = OkHttpClient.Builder().apply {
        interceptor?.let { addInterceptor(interceptor = it) }
        connectTimeout(timeout = 1, unit = TimeUnit.MINUTES)
        writeTimeout(timeout = 30, unit = TimeUnit.SECONDS)
        readTimeout(timeout = 10, unit = TimeUnit.MINUTES)
    }.build()
}