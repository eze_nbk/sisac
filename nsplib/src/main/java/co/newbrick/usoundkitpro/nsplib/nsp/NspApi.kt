package co.newbrick.usoundkitpro.nsplib.nsp

import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.concurrent.Executors


internal interface NspApi {

    @Headers( "Content-Type: application/json; charset=utf-8")
    @POST("{url}")
    fun post(
        @Path("url") url: String,
        @Body body: RequestBody
    ): Call<ResponseBody>

    companion object {
        fun create(baseUrl: String, client: OkHttpClient, factory: Converter.Factory?): NspApi =
            Retrofit.Builder()
                .baseUrl(baseUrl.toHttpUrlOrNull()!!)
                .client(client)
                .addConverterFactoryIfAvailable(factory)
                .callbackExecutor(Executors.newSingleThreadExecutor())
                .build()
                .create(NspApi::class.java)

        /** Add converter factory for serialization and deserialization of objects.  */
        private fun Retrofit.Builder.addConverterFactoryIfAvailable(factory: Converter.Factory?) =
            factory?.let { addConverterFactory(factory) } ?: this

        fun create(baseUrl: String, client: OkHttpClient): NspApi =
            Retrofit.Builder()
                .baseUrl(baseUrl.toHttpUrlOrNull()!!)
                .client(client)
                .callbackExecutor(Executors.newSingleThreadExecutor())
                .build()
                .create(NspApi::class.java)
    }
}