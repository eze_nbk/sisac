package co.newbrick.usoundkitpro.nsplib.post

import co.newbrick.usoundkitpro.nsplib.nsp.NspInterface
import co.newbrick.usoundkitpro.nsplib.nsp.ResponseCode
import co.newbrick.usoundkitpro.nsplib.nsp.ResponseStatus
import co.newbrick.usoundkitpro.nsplib.utils.GsonParser
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServerPoster {

    val nspApi: NspInterface = object : NspInterface {}

    inline fun <X, reified Z> post(
        serverUrl: String,
        obj: X,
        crossinline callback: ((ResponseStatus<Z>) -> Unit)
    ) = nspApi.post(
        serverUrl = serverUrl,
        json = GsonParser.toJson(obj),
        callback = object : Callback<ResponseBody> {
            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                if(response.isSuccessful && response.code() < 400) {
                    response.body()?.let {
                        if(!Z::class.java.isInstance(Unit)) {
                            try {
                                callback.invoke(ResponseStatus(
                                    status = ResponseCode.SUCCESS,
                                    response = GsonParser.fromJson(
                                        src = it.string()
                                    )
                                ))
                            } catch(e: Exception) {
                                e.printStackTrace()
                                callback.invoke(ResponseStatus(
                                    status = ResponseCode.ERROR_INTERNAL,
                                    response =  null
                                ))
                            }
                        } else callback.invoke(ResponseStatus(
                            status = ResponseCode.ERROR_INTERNAL,
                            response =  null
                        ))
                    } ?: callback.invoke(ResponseStatus(
                        status = ResponseCode.ERROR_CONNECTION,
                        response =  null
                    ))
                } else callback.invoke(ResponseStatus(
                    status = ResponseCode.ERROR_INTERNAL,
                    response =  null
                ))
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                callback.invoke(ResponseStatus(
                    status = ResponseCode.ERROR_CONNECTION,
                    response =  null
                ))
            }
        }
    )


    /**
     * Repository interface for use in client
     */
    interface Repository {
        /**
         * @return non-singleton new instance of [ServerPoster].
         */
        fun getServerPoster() = ServerPoster()
    }
}