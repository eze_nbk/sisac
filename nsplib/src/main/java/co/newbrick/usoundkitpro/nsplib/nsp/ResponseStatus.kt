package co.newbrick.usoundkitpro.nsplib.nsp

class ResponseStatus<T>(val status: Int, val response: T?, val errorMessage: String = "") {

    fun isSuccessful() = status == ResponseCode.SUCCESS
}
