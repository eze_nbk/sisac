package co.newbrick.usoundkitpro.nsplib.nsp

object ResponseCode {
//    const val SUCCESS_NOT_REGISTERED_IN_DEVICE = 1
    const val SUCCESS = 0
    const val ERROR_INTERNAL = -4
    const val ERROR_CONNECTION = -5
}